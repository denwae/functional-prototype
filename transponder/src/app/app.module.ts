import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {OverviewComponent} from './overview/overview.component';
import {MatrikelSearchComponent} from './overview/matrikel-search/matrikel-search.component';
import {SearchComponent} from './overview/search/search.component';
import {ResultsComponent} from './overview/results/results.component';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {MatTableModule} from '@angular/material/table';
import {MatButtonModule} from '@angular/material/button';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatIconModule} from '@angular/material/icon';
import {MatInputModule} from '@angular/material/input';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatCardModule} from '@angular/material/card';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatChipsModule} from '@angular/material/chips';
import {BorrowModalComponent} from './overview/borrow-modal/borrow-modal.component';
import {SignaturePadModule} from 'angular2-signaturepad';
import {MAT_DIALOG_DATA, MatDialogModule, MatDialogRef} from '@angular/material/dialog';
import {TransponderService} from './services/transponder.service';
import { ReturnModalComponent } from './overview/return-modal/return-modal.component';
import { MatSnackBarModule} from '@angular/material/snack-bar';
import {MatTooltipModule} from '@angular/material/tooltip';



@NgModule({
  declarations: [
    AppComponent,
    OverviewComponent,
    MatrikelSearchComponent,
    SearchComponent,
    ResultsComponent,
    BorrowModalComponent,
    ReturnModalComponent
  ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        NoopAnimationsModule,
        MatTableModule,
        MatButtonModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        FormsModule,
        MatCardModule,
        MatAutocompleteModule,
        MatChipsModule,
        ReactiveFormsModule,
        AppRoutingModule,
        SignaturePadModule,
        MatDialogModule,
        MatSnackBarModule,
        MatTooltipModule,
    ],
  providers: [
    {
      provide: MAT_DIALOG_DATA,
      useValue: {}
    },
    {
      provide: MatDialogRef,
      useValue: {}
    },
    TransponderService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
