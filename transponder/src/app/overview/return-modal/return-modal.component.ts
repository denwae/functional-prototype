import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {TransponderModel} from "../../shared/models/transponder.model";
import {TransponderService} from "../../services/transponder.service";
import {SnackbarService} from "../../services/snackbar.service";

@Component({
  selector: 'app-return-modal',
  templateUrl: './return-modal.component.html',
  styleUrls: ['./return-modal.component.css']
})
export class ReturnModalComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public borrowedTransponder: TransponderModel,
              private snackbarService: SnackbarService,
              private dialogRef: MatDialogRef<ReturnModalComponent>,
              private transponderService: TransponderService) {
  }

  ngOnInit(): void {
  }

  returnTransponder() {
    this.transponderService.returnTransponder(this.borrowedTransponder);
    this.dialogRef.close();
    this.snackbarService.showSuccesSnackbar("Transponder " + this.borrowedTransponder.id + " erfolgreich zurückgegeben!");
  }

}
