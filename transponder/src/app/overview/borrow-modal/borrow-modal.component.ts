import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {SignaturePad} from 'angular2-signaturepad';
import {UserService} from '../../services/user.service';
import {TransponderService} from '../../services/transponder.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {UserModel} from '../../shared/models/user.model';
import {TransponderModel} from '../../shared/models/transponder.model';
import {SnackbarService} from '../../services/snackbar.service';
import {SearchService} from '../../services/search.service';

export interface BorrowData {
  user: UserModel;
  transponder: TransponderModel;
}

@Component({
  selector: 'app-borrow-modal',
  templateUrl: './borrow-modal.component.html',
  styleUrls: ['./borrow-modal.component.css']
})
export class BorrowModalComponent implements OnInit {

  @ViewChild(SignaturePad) signaturePad: SignaturePad;


  signaturePadOptions: object = {
    canvasWidth: 500,
    canvasHeight: 300
  };

  constructor(private transponderService: TransponderService,
              private snackbarService: SnackbarService,
              private dialogRef: MatDialogRef<BorrowModalComponent>,
              @Inject(MAT_DIALOG_DATA) public data: BorrowData,
              private searchService: SearchService
  ) {}

  ngOnInit(): void {
    console.log(this.data.transponder);
  }

  clearSigPad(): void {
    this.signaturePad.clear();
  }

  getBorrowedTransponders(user: UserModel): Array<TransponderModel> {
    return this.transponderService.getBorrowedTransponders(user);
  }

  borrowTransponder(): void {
    this.transponderService.borrowTransponder(this.data.transponder, this.data.user);
    this.dialogRef.close();
    this.snackbarService.showSuccesSnackbar(`Transponder ${this.data.transponder.id} erfolgreich ausgeliehen!`);
    this.searchService.setRoom(null);
    this.searchService.setUser(null);
  }

}
