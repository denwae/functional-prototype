import {Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import {SearchService} from "../../services/search.service";
import {UserModel} from "../../shared/models/user.model";
import {FormControl} from "@angular/forms";
import {Observable} from "rxjs";
import {UserService} from "../../services/user.service";
import {map, startWith} from "rxjs/operators";
import {RoomModel} from "../../shared/models/room.model";
import {RoomService} from "../../services/room.service";

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  @ViewChild('userInput') userInput: ElementRef;

  public value: RoomModel;
  myControl = new FormControl();
  filteredOptions: Observable<RoomModel[]>;

  constructor(public roomService: RoomService, public searchService: SearchService) { }

  ngOnInit(): void {
    this.filteredOptions = this.myControl.valueChanges
      .pipe(
        startWith(''),
        map(value => this._filter(value))
      );
  }

  _filter(val): RoomModel[] {
    if (val === null || val === undefined || typeof val != "string") return []
    return val ? this.roomService.roomList.filter(x => x.name.startsWith(val)) : this.roomService.roomList;
  }

  displayFn(room: RoomModel) {
    return room ? room.name : room;
  }

}
