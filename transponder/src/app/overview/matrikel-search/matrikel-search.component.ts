import {Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import {UserService} from '../../services/user.service';
import {FormControl} from '@angular/forms';
import {UserModel} from '../../shared/models/user.model';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import {SearchService} from '../../services/search.service';

@Component({
  selector: 'app-matrikel-search',
  templateUrl: './matrikel-search.component.html',
  styleUrls: ['./matrikel-search.component.css']
})
export class MatrikelSearchComponent implements OnInit {

  @ViewChild('userInput') userInput: ElementRef;

  public value: UserModel;
  myControl = new FormControl();
  filteredOptions: Observable<UserModel[]>;

  constructor(public userService: UserService, public searchService: SearchService) { }

  ngOnInit(): void {
    this.filteredOptions = this.myControl.valueChanges
      .pipe(
        startWith(''),
        map(value => this._filter(value))
      );
  }

  _filter(val): UserModel[] {
    if (val === null || val === undefined || typeof val !== 'string') { return []; }
    return val ? this.userService.userList.filter(x => x.lastName.toLowerCase().includes(val.toLowerCase()) ||
      x.firstName.toLowerCase().includes(val.toLowerCase()) || (x.matrikelNummer != null &&
      x.matrikelNummer.toString().includes(val)) || x.campusID.toLowerCase().includes(val.toLowerCase())) : this.userService.userList;
  }

  displayFn(user: UserModel): any {
    return user ? user.firstName + ' ' + user.lastName : user;
  }
}
