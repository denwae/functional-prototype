import {ChangeDetectorRef, Component, Input, OnInit} from '@angular/core';
import {TransponderModel} from '../../shared/models/transponder.model';
import {SearchService} from '../../services/search.service';
import {MatDialog} from '@angular/material/dialog';
import {BorrowModalComponent} from '../borrow-modal/borrow-modal.component';
import {TransponderService} from '../../services/transponder.service';
import {UserModel} from '../../shared/models/user.model';
import {ReturnModalComponent} from '../return-modal/return-modal.component';

@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.css']
})
export class ResultsComponent implements OnInit {
  displayedColumns: string[] = ['transponder', 'rooms', 'status', 'roomAuthority', 'actions'];

  constructor(public searchService: SearchService,
              private transponderService: TransponderService,
              private matDialog: MatDialog) {
  }

  ngOnInit(): void {
  }

  borrowTransponder(transponder: TransponderModel): void {
    if (this.isUserSelected()) {
      this.matDialog.open(BorrowModalComponent, {
        data: {user: this.searchService.getUser(), transponder}
      });
    }
  }

  isUserSelected(): boolean {
    return this.searchService.getUser() !== null;
  }

  returnTransponder(transponder: TransponderModel): void {
    this.matDialog.open(ReturnModalComponent, {
      data: transponder
    });
  }

  sortChange(): void {
    this.searchService.sortChange();
  }
}
