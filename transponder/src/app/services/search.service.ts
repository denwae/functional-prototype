import {Injectable} from '@angular/core';
import {TransponderModel} from '../shared/models/transponder.model';
import {UserModel} from '../shared/models/user.model';
import {RoomModel} from '../shared/models/room.model';
import {transponders} from '../shared/init-data';

@Injectable({
  providedIn: 'root'
})
export class SearchService  {

  private allTransponders = transponders;
  public results: TransponderModel[];

  public sorted = 'not';

  private userConstraint: UserModel = null;
  private roomConstraint: RoomModel = null;

  constructor() {
    this.filter();
  }


  filterByUser(trans: TransponderModel[]): TransponderModel[] {
    if (this.userConstraint == null) { return trans; }
    else { return trans.filter(t => t.authorizedUsers.includes(this.userConstraint)); }
  }

  filterByRoom(trans: TransponderModel[]): TransponderModel[] {
    if (this.roomConstraint == null) { return trans; }
    else { return trans.filter(t => t.rooms.includes(this.roomConstraint)); }
  }

  setRoom(room: RoomModel): void {
    this.roomConstraint = room;
    this.filter();
  }

  setUser(user: UserModel): void {
    this.userConstraint = user;
    this.filter();
  }

  getUser(): UserModel {
    return this.userConstraint;
  }

  filter(): void {
    this.results = this.filterByUser(this.filterByRoom(this.allTransponders));
  }

  sortChange(): void {
    if (this.sorted === 'not') {
      this.results = [...this.results.sort((a: TransponderModel, b: TransponderModel) => {
        if ((a.status.borrowed && b.status.borrowed) || (!a.status.borrowed && !b.status.borrowed)) {
          return 0;
        } else if (a.status.borrowed && !b.status.borrowed) {
          return 1;
        } else if (!a.status.borrowed && b.status.borrowed) {
          return -1;
        }
      })];
      this.sorted = 'asc';
    } else if (this.sorted === 'asc') {
      this.results = [...this.results.sort((a: TransponderModel, b: TransponderModel) => {
        if ((a.status.borrowed && b.status.borrowed) || (!a.status.borrowed && !b.status.borrowed)) {
          return 0;
        } else if (a.status.borrowed && !b.status.borrowed) {
          return -1;
        } else if (!a.status.borrowed && b.status.borrowed) {
          return 1;
        }
      })];
      this.sorted = 'desc';
    }  else if (this.sorted === 'desc') {
      this.filter();
      this.sorted = 'not';
    }
  }
}
