import { Injectable } from '@angular/core';
import {rooms} from '../shared/init-data';

@Injectable({
  providedIn: 'root'
})
export class RoomService {

  public roomList = rooms;

  constructor() { }
}
