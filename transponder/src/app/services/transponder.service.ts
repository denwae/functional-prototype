import {Injectable} from '@angular/core';
import {TransponderModel} from '../shared/models/transponder.model';
import {transponders} from '../shared/init-data';
import {UserModel} from "../shared/models/user.model";
import {TransponderStatusModel} from "../shared/models/transponder-status.model";

@Injectable()
export class TransponderService {
  transponders: Array<TransponderModel> = transponders;

  constructor() {
  }

  getTransponder(number: number): TransponderModel {
    return this.transponders.find(transponder => {
      if (number === transponder.id)
        return transponder
    })
  }

  getBorrowedTransponders(user: UserModel): Array<TransponderModel> {
    let borrowedTransponders = Array<TransponderModel>();

    transponders.forEach(transponder => {
      if (transponder.status.user === user) {
        borrowedTransponders.push(transponder)
      }
    });

    return borrowedTransponders;
  }

  borrowTransponder(transponder: TransponderModel, user: UserModel) {
    transponder.status = new TransponderStatusModel(true, new Date(Date.now()), user)
  }

  returnTransponder(transponder: TransponderModel) {
    transponder.status = new TransponderStatusModel(false, null,null)
  }

}
