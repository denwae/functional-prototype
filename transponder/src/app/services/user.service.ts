import {Injectable} from '@angular/core';
import {UserModel} from '../shared/models/user.model';
import {users} from '../shared/init-data';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  public userList: UserModel[] = users;

  constructor() {
  }

  getUser(matNum: number): UserModel {
    return this.userList.find(user => {
      if (user.matrikelNummer === matNum) {
        return user;
      }
    });

  }

}
