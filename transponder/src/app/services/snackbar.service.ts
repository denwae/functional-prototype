import { Injectable } from '@angular/core';
import {MatSnackBar} from "@angular/material/snack-bar";

@Injectable({
  providedIn: 'root'
})
export class SnackbarService {

  constructor(private snackBar: MatSnackBar) { }

  showSuccesSnackbar(message: string) {
    this.snackBar.open(message,null, {
      duration: 3000,
      horizontalPosition: "right",
      verticalPosition: "top",
      panelClass: "snackBarSuccess"
    })
  }

}
