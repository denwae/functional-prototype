import {RoomModel} from "./room.model";
import {UserModel} from "./user.model";


export class TransponderStatusModel {
  borrowed: boolean;
  borrowTime: Date;
  user: UserModel;

  constructor(borrowed: boolean,
              borrowTime: Date,
              user: UserModel) {
    this.borrowed = borrowed;
    this.borrowTime = borrowTime;
    this.user = user;
  }

  toString() {
    if (!this.borrowed) {
      return "frei"
    } else if (this.borrowTime != null && this.user != null) {
      return "Ausgeliehen von "  + this.user.toString() +
        " seit " + this.borrowTime.toLocaleDateString('de-DE') + " um " + this.borrowTime.toLocaleTimeString('de-DE')
    } else {
      throw Error("TransponderStatus is borrowed, but doesn't have a correlated room and borrowTime!")
    }

  }
}
