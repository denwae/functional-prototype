import {RoomModel} from "./room.model";
import {UserModel} from "./user.model";
import {TransponderStatusModel} from "./transponder-status.model";
import {Role} from "./role.model";


export class TransponderModel {
  id: number;
  rooms: RoomModel[];
  status: TransponderStatusModel;
  roomAuthority: UserModel;
  roomAuthorityHelper: UserModel[];
  authorizedUsers: UserModel[];

  constructor(id,
              rooms,
              status,
              roomAuthority,
              roomAuthorityHelper,
              authorizedUsers) {
    this.id = id;
    this.rooms = rooms;
    this.status = status;
    if (roomAuthority.role == Role.DOZENT)
      this.roomAuthority = roomAuthority;
    else throw Error("Only Dozenten can be room authorities")
    this.roomAuthorityHelper = roomAuthorityHelper;
    this.authorizedUsers = authorizedUsers;
  }

  getRoomsAsString() {
    return this.rooms.map((room) => {return room.name}).join(", ")
  }
}
