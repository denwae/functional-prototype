import {Role} from "./role.model";

export class UserModel {
  id: number;
  matrikelNummer: number;
  campusID: string;
  firstName: string;
  lastName: string;
  role: Role;

  constructor(id, matNum, campusID, first, last, role) {
    this.id = id;
    this.matrikelNummer = matNum;
    this.campusID = campusID;
    this.firstName = first;
    this.lastName = last;
    this.role = role;
  }

  toString() {
    return this.firstName + " " + this.lastName;
  }

}
